# 4. SPI communication

This week I worked in making communication betwwen two boards using a protocol of communication. I chose the SPI.
The boards used were Arduino Uno and AVR-MT128 da Olimex. I control the LED available on the Olimex. The Arduino receive commands for the LED via USB and relays it to the Olimex.

## SPI Protocol

The explanation of this protocol and code that I used as referecence for this work is available on: http://www.gammon.com.au/spi

### Set up of the Arduino IDE to program Olimex
All the steps needed to set up the Arduino IDE for Olimex can be found on:
https://github.com/fablaboulu/olimex_arduino/blob/master/README.md.

### Set up of the Arduino IDE to program Arduino Uno
In the Arduino IDE, select Tools->Board->Arduino Uno and Tools->Board->Programmer->ArduinoISP.


### Arduino UNO pinout
The pinout of the Arduino is in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/arduino_uno_pinout.jpg" width="400" height="400" />

I used the USB to control the Arduino by serial communication. The SS, MOSI, MISO, SCK are the pins 10, 11, 12 and 13, respectively.

### Olimex Schematics
The full schematics of the development board can be found in: https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128-SCH-REV-A.pdf

As you can see it is used a relay to control de LED. The relay is connected to the pin PA6 of the microcontroller.

In the figure below, you can see where are the pins SS, MISO and MOSI.

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/extension_2.PNG" width="200" height="200" />

The SS, MOSI and MISO pins of the microcontroller are the pins 6, 7 and 8 of the EXT2, respectively.

In the figure below, you can see where is the pin SCK.

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/icsp_sck.PNG" width="200" height="200" />

The SCK pin of the microcontroller is the pin 7 of the ICSP.

I am using the Arduino hardware package MegaCore. So I need to check in the documentation what is the number used to represent a respective pin of the microcontroller in the Arduino IDE.
It can be seen in:

<img src="https://camo.githubusercontent.com/2e9fe11875f8ca873e6d9ed5c9602acdaf5c089f/68747470733a2f2f692e696d6775722e636f6d2f737765524a73332e6a7067" width="400" height="400" />

Therefore, the pin number I should use for relay (LED) is the 38. The pins used for the SPI are already set correctly by the library spi.h provided by MegaCore.

### Wire Connection

The wires were connect as is shown in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/week_4_wires.jpg" width="400" height="400" />

The SS, MOSI, MISO, SCK, ground are the red, yellow, green, blue and brown wires, respectively.


### Boards code

The code used for the Arduino Uno (SPI Master) is below:

```
// include the library code:
#include <SPI.h>

void setup() {
  // Initializes the Serial connection.
  Serial.begin(9600);
  digitalWrite(SS, HIGH);  // ensure SS stays high for now
  SPI.begin ();

  // Slow down the master a bit
  SPI.setClockDivider(SPI_CLOCK_DIV8);
}
int count = 0;  
void loop() {
  // check for incoming serial data:
  if (Serial.available())
  {
    char c = Serial.read();
    digitalWrite(SS, LOW);    // SS is pin 10
    SPI.transfer (c);
     // disable Slave Select
    digitalWrite(SS, HIGH);
  }
}
```

The code of the Olimex (SPI slave) is below:

```
#include <SPI.h>

char buf;
volatile byte pos;
volatile boolean process_it;
const int ledPin =  38;      // the number of the LED pin

void setup() {
// initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
// turn on SPI in slave mode
  SPCR |= bit (SPE);
// have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);
  
  // get ready for an interrupt 
  pos = 0;   // buffer empty
  process_it = false;

  // now turn on interrupts
  SPI.attachInterrupt();
}
// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register
  buf = c;   
  process_it = true;
      
}  // end of interrupt routine SPI_STC_vect

void loop() {
  if (process_it){
   if(buf == 'a') {
        // turn LED on:
        digitalWrite(ledPin, HIGH);
    } else if(buf == 'b') {
       // turn LED off:
        digitalWrite(ledPin, LOW);
    }
    process_it = false;
  }  // end of flag set

}
```

### Deployment

I uploaded the code for both boards and used serial monitor of the Arduino IDE to control them. The result can be seen in the video below:

<iframe src="https://player.vimeo.com/video/419534263" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Group Assigment

We were supposed to send a message between two projects. We decided to use my individual assignment of the week 4 but now the boards are controlled by a sensor that it is availabe somewhere via the internet.
In this case, my individual assignment can be seen as an alarm (Olimex) controlled by an alarm controller (Arduino Uno). The alarm controller is triggered by a sensor which is somewhere connected to the internet. 
The sensor is connected to another Arduino Uno

Here is a representation of how the connections are done:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/diagram_group_ass_4.png" width="1000" height="50" />

The serial connections are USB.




### Board code

The code of the sensor is below:
```
const int sensor =  2;      // the number of the LED pin
int sensorState = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sensor, INPUT); // Pull up resistor
  digitalWrite(sensor, HIGH);
}

void loop() {
  sensorState = digitalRead(sensor);
  if(sensorState == HIGH){
      Serial.print('a');
  } else {
      Serial.print('b');
  }
  // put your main code here, to run repeatedly:
  delay(20);
}
```
It is a simple code where it is checked whether the circuit is closed. We used the pin number 2 of the Arduino Uno for this purpose. To close the circuit, a wire from the pin 2 need to be connected to the ground.
When the circuit is closed, the Arduido send by the serial the character 'b' which is the normal state of the system. When the circuit is open the Arduino sends by the Serial the character 'a' which means that the alarm should be triggered.

The code of the alarm and the alarm controller are in my individual assignment.

### Python application
I used python as backend of my application. I created a simple HTTP server and HTTP client to be able to control the alarm.
The version of my Python is 3.8.2.
Requirements:
-Flask
-pySerial

### HTTP Client
The HTTP client reads the serial and sends what was received to the HTTP server through a HTTP request (POST).
The HTTP client code is below:
```
import serial
import requests
url = 'http://85.23.49.200/'

ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

while True:
    command = ser.read().decode() # Read command serial
    requests.post(url+command) # Send command using REST API
ser.close()
```

### HTTP Server
The HTTP server receives the information from the HTTP client and send the information to the alarm controller via serial.
The code of the HTTP server is below:
```
from flask import Flask
import serial
import time
from flask import render_template

server = Flask(__name__)

ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

# The method POST is a HTTP request done a button is pressed.
@server.route('/a', methods=['POST'])
def led_on():
    ser.write(b'a')     # Turn on the LED
    return ('', 204) # 204 code is used because no action is required from the browser
@server.route('/b', methods=['POST'])
def led_off():
    ser.write(b'b')     # Turn off the LED
    return ('', 204) # 204 code is used because no action is required from the browser
def main():
    time.sleep(2) # Wait the establishment of the serial connection
    # host 0.0.0.0 as I want my service to be acessible from external connections
    server.run(host='0.0.0.0', port=80, debug=False,)
    ser.close()             # close port

if __name__ == '__main__':
    main()
```

### Deployment
We recorded a video where the sensor and the alarm were in different houses. So, we used the internet to make the communication.
The video is below:

<iframe src="https://player.vimeo.com/video/419538095" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>