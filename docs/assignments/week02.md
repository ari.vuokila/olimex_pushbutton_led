# 2. Project management

### ABSTRACT
In this work, I made serial communcation between my computer and an Arduino Uno. I connected a digital port of the Arduino Uno to the development board AVR-MT128 da Olimex to use the LED.
I wanted to control the led.

### Application
The social isolation limited the socialization with others. I created a simple web page where the a person could answer my questions by pressing one of the three available buttons. The buttons are "No!", "Yes", "A lot!" and when pressed it makes the LED blink one, two and three times, respectively. 
To use the application, I made a call with a person and asked to access the adress where my application is running. The person answered questions made by me with the buttons.

### Set up of the Arduino IDE to program Olimex
All the steps needed to set up the Arduino IDE for Olimex can be found on:
https://github.com/fablaboulu/olimex_arduino/blob/master/README.md.

### Set up of the Arduino IDE to program Arduino Uno
In the Arduino IDE, select Tools->Board->Arduino Uno and Tools->Board->Programmer->ArduinoISP.


### Arduino UNO pinout
The pinout of the Arduino is in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/arduino_uno_pinout.jpg" width="400" height="400" />

I used the USB to control the Arduino by serial communication. The pin 13 (PB5) was used to send a digital signal to the Olimex.

### Olimex Schematics
The full schematics of the development board can be found in: https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128-SCH-REV-A.pdf

As you can see it is used a relay to control de LED. The relay is connected to the pin PA6 of the microcontroller.
I use the ADC ports to receive the signal from the Arduino. I use the pin 6 to receive the signal and the pin 2 as ground reference.
The pin 6 corresponds to the pin ADC3 of the microcontroller.

I am using the Arduino hardware package MegaCore. So I need to check in the documentation what is the number used to represent a respective pin of the microcontroller in the Arduino IDE.
It can be seen in:

<img src="https://camo.githubusercontent.com/2e9fe11875f8ca873e6d9ed5c9602acdaf5c089f/68747470733a2f2f692e696d6775722e636f6d2f737765524a73332e6a7067" width="400" height="400" />

Therefore, the pin numbers I should use for the digital port and for the relay are 48 and 38, respectively.
### LED and wire connection.
The wire connection can be seen in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/wire_connection.jpg" width="400" height="400" />

The blue and green wire are the digital and ground connection, respectively.

The led used is in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/led_olimex.jpg" width="200" height="400" />

## Code used in Arduino IDE

The code used for the Arduino to receive the commands from the USB (serial) and transmitte the digital signal to Olimex is below:

```
const int digitalPin =  13; 

void setup() {
  // digital mode as OUTPUT as we will transmit a signal.
  pinMode(digitalPin, OUTPUT);
  // Default value of the signal is LOW 
  digitalWrite(digitalPin, LOW);
  // Initializes the Serial connection.
  Serial.begin(9600);
}
  
void loop() {
  // check for incoming serial data:
  if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    //If 'a' is received from the serial connection, change digital pin to HIGH voltage
    if (inChar == 'a'){ 
        digitalWrite(digitalPin, HIGH);
    //If 'b' is received from the serial connection, change digital pin to LOW voltage        
    } else if (inChar == 'b'){
        digitalWrite(digitalPin, LOW); 
    }
  }
}
```
The Olimex code to control the LED/relay based on the input digital signal is below:

```
const int inputADC = 48;     // the number of the input ADC pin
const int ledPin =  38;      // the number of the LED pin

// variables will change:
int inputADCState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the digital pin as an input:
  pinMode(inputADC, INPUT);
}

void loop() {
  // read the state of the ditital pin value:
  inputADCState = digitalRead(inputADC);

  // check if the the digital input is HIGH
  if (inputADCState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```

With the wires connected correctly and this code, it is possible to control the LED by the serial communcation.
Now I just needed an application to control the LED.

## Application
I used python as backend of my application. I created a simple web server and web page to be able to control the LED.
The version of my Python is 3.8.2.
Requirements:
-Flask
-pySerial

I used Flask to run a HTTP server and pySerial to make the serial communication with the Arduino IDE.
The python code used is bellow:
```
from flask import Flask
import serial
import time
from msvcrt import getch
from flask import render_template

server = Flask(__name__)

ser = serial.Serial('COM8', 9600)  # open serial port

def blink(num):
    for i in range(num):
        ser.write(b'a')     # write a string
        time.sleep(0.2)      
        ser.write(b'b')     # write a string
        time.sleep(0.2)      

@server.route('/Yes', methods=['POST'])
def yes():
    blink(2)
    return ('', 204)
@server.route('/No', methods=['POST'])
def no():
    blink(1)
    return ('', 204)
@server.route('/A_lot', methods=['POST'])
def a_lot():
    blink(3)
    return ('', 204)
@server.route('/', methods=['GET'])
def index():
    return render_template("index.html")

def main():
    time.sleep(2)
    server.run(host='0.0.0.0', port=80, debug=False,)
    ser.close()             # close port

if __name__ == '__main__':
    main()
```
COM8 was the serial port used for my computer.
index.html is a simple html page used for the application. The file should be inside a folder called templates. This folder should be in the same direcotry than the application .py
The index.html is below:
```
<!DOCTYPE html>
<html lang="en">
        <head>
                  <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>LED answerer!</title>
        </head>

        <body>
                  <h1 style="color: black", align="center">Use the buttons to answer the questions</h1>
                  <form action="/No" method="post" align="center">
                         <input type="submit" id="No" value="No!" style="height:75px; width:100px">
                  </form>
                  <p></p>
                  <form action="/Yes" method="post" align="center">
                         <input type="submit" id="Yes" value="Yes!" style="height:75px; width:100px">
                  </form>
                  <p></p>
                  <form action="/A_lot" method="post" align="center">
                         <input type="submit" id="A_lot" value="A lot!!" style="height:75px; width:100px">
                  </form>
        </body>

</html><!DOCTYPE html>
```
When an user press a button of the page, The browser sends an event (In this case, a HTTP request) to the web server. The web server handle the event and send commands to the Arduino IDE through the serial connection to blink the LED. Thenumber of blinks depends on the button pressed.

### Deployment
I tested the application locally and with a friend in Russia. I asked her some questions in a call and she answered pressing the buttons.
The video below show it working locally (127.0.0.1):

<iframe src="https://player.vimeo.com/video/402512157" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

It can require knowledge of computer networks to make it work externally. The configurations depend on your network and computer.
