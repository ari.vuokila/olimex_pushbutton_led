# 3. Trash waste volume measurer

##What will it do?
In Finland, it is common to have public garbage dump that are big holes in the ground. They are divided in different types of waste.
Periodically a garbage truck needs to come to where is the garbage dump and take the wast out.
The idea is to use sensors to check the volume of trash filling the hole. If this information, it's possible to organize the schedules of the trucks in a effecient way. 

##Who's done what beforehand?
Some companies already has a product that does that. For example:
https://sensoneo.com/product/smart-sensors/
https://www.urbiotica.com/en/producto/u-dump-m2m-2/

Usually, ultrasonic sensors are used to measure the volume. 

##What will you design?
A system where it is sent to the waste management the information of the filling of all garbage dump. If this information, it's possible to know when the garbage should be taken away and also it's possible to predict the best momento to take ther garbage out.
With an APP, it would be possible for citzens check the closest garbage dump available for a type of waste with space.

##What materials and components will be used?
Microcontroller, battery, ultasonic sensor, 5g modem.

##Where will come from?
China

##How much will they cost?
I do not know

##What parts and systems will be made?
none

##What processes will be used?
I do not know

##What questions need to be answered?
Is there a market for this product?
Can it be used for other applications?
How efficient will be the trucks with the information provided by the system?

##How will it be evaluated?
The accurace of the system and how much it increases the efficiency of the trucks that take the waste away.
