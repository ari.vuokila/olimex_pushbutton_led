# 1. Control LED using a button

### ABSTRACT
In this work, I set up arduino IDE and made a simple application.
### Application
Program the development board AVR-MT128 to light up its LED when a determined
button is pressed.
### Set up of the Arduino IDE and cable connections
All the steps needed to set up the Arduino IDE for Olimex can be found on:
https://github.com/fablaboulu/olimex_arduino/blob/master/README.md

From this documentation I found other useful links to code the application.
### The button and the LED
In the figure you can see the buttons and LED available:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week01/led_button_photo.jpg" width="200" height="400" />

I decided to use the button B1 to light up the LED.
### Schematics of the AVR-MT128
The full schematics of the development board can be found in: https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128-SCH-REV-A.pdf

The LED is activated by the relay as it can be seen in the figure:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week01/led_relay.PNG" width="200" height="400" />

The transistor T3 is used as a switch. The relay is used to control the switch.
The switch is considered ON with high voltage and OFF with low voltage.

This figure shows to which microcontroller's pins the relay (LED) and the button B1 are connected to:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week01/microcontroller_pins.PNG" width="200" height="400" />

The button B1 and relay are connected to the pin PA0 and PA6, respectively.
Notice that the buttons of this development board use pull-up resistor. It will be important while doing the code for the apliccation.

I am using the Arduino hardware package MegaCore. So I need to check in the documentation what is the number used to represent a respective pin of the microcontroller.
It can be seen in:

<img src="https://camo.githubusercontent.com/2e9fe11875f8ca873e6d9ed5c9602acdaf5c089f/68747470733a2f2f692e696d6775722e636f6d2f737765524a73332e6a7067" width="400" height="400" />

Therefore, the pin numbers I should use for the button and for the relay are 44 and 38, respectively.

### Code Used
I used a example code for button available in Arduido IDE. It is available on File->Examples->Digital->Button.
This examples is used to light up a LED when a button is pressed. It is exacatly the application that I am looking for.
I changed the variables buttonPin and ledPin to 44 and 38, respectively.
The condition where the button state is checked was changed as well because the example code consider pull-down resistor but this development board has pull-up resistor.
The code is below: 
```
const int buttonPin = 44;     // the number of the pushbutton pin
const int ledPin =  38;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is LOW (pull-up resistor):
  if (buttonState == LOW) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```

### Deployment
The outcome of the steps folowed is the video below:

<iframe src="https://player.vimeo.com/video/402511754" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

